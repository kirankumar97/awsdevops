#   ---------CHURN ANALYSIS--------  #
#!/usr/bin/python3
#Loading Packages
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
from sklearn.metrics import accuracy_score,confusion_matrix,recall_score,precision_score
import pandas as pd
import numpy as np
from sklearn.model_selection import cross_val_score,train_test_split,KFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from matplotlib import pyplot as plt



#Region-1:-------------------Training---------------------

#Read From excel file
data1=pd.read_excel('dataset.xlsx')

#-----Input feature Analysis------
#Histogram
data2=data1.drop(['Application_ID'],axis=1)
data2.hist()
plt.show()

#Boxplot to determine the outliers in the training data
data2.plot(kind='box',subplots=True,layout=(5,5),sharex=False,sharey=False)
plt.show()

#Convert to Matrix
data=data1.values

#-------Preprocessing-------
# filling NAN values with most frequent value in column
df = data2.fillna(df.mode().iloc[0])
#Encode Categorical Columns using Label Encoder
le=LabelEncoder()
for col in df.columns.values:
    if df[col].dtypes=='object':
        le.fit(df[col].values)
        df[col]=le.transform(df[col])


X=df.iloc[:,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24]]
#Target Column
y=df.iloc[:,14].astype(int)
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.1)
#Compairing Algorithms to get the best Model

kfold=KFold(10,random_state=7)
models=[]
models.append(("KNN",KNeighborsClassifier()))
models.append(("SVM",SVC()))
models.append(("NB",GaussianNB()))
models.append(("LG",LogisticRegression()))
models.append(("CART",DecisionTreeClassifier()))
models.append(("RF",RandomForestClassifier()))
results=[]
names=[]
for name,model in models:
	kfold=KFold(n_splits=10,random_state=7) 
	v=cross_val_score(model,X_train,y_train,cv=kfold,scoring='accuracy')
	results.append(v)
	names.append(name)
	print(name)
	print(v.mean())
fig=plt.figure()
fig.suptitle('Algorithm Comparison')
ax=fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels(names)
plt.show()


#Fit the best Model with Training Data
from sklearn.model_selection import GridSearchCV
rf=RandomForestClassifier()
gs=GridSearchCV(rf,param_grid={'max_depth':[3,6,9,12],'n_estimators':[100,200,400,600]},cv=10,scoring='accuracy')
gs.fit(X,y)
p=gs.predict(X)
print(gs.best_score_)
print(gs.best_estimator_.n_estimators)
print(gs.best_estimator_.max_depth)
'''
'''


        


#Region-2:-----------------Validation-------------------

# create an object of the best model then use the command
#model=RandomForestClassifier(max_depth=...,n_estimators=..)
#model.fit(X_train,y_train)
#p1=model.predict(X_test)
#print(classification_report(y_test,p1))
#print(confusion_matrix(y_test,p1))
